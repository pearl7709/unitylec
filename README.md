# Unity講習会用プロジェクト

講習会用のプロジェクトです．

 1. HelloWorld
 2. ShootingGame

の2シーンで構成されています．


### クレジット表記

![ライセンスロゴ](./Light_Frame.png)

このコンテンツは、『[ユニティちゃんライセンス](http://unity-chan.com/contents/license_jp/)』で提供されています。