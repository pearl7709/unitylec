﻿using UnityEngine;
using System.Collections;

public class HelloWorldScript_CS : MonoBehaviour {

	public GameObject ball;	//ball
	public int height = 5; //height a ball fall from

	// Use this for initialization
	void Start () {
		//print ("Hello, world!");

		/* create ball array */
		int arrayHead= -2;	//Ball array starts from left
		for( int i = 0; i < 5; i++ ){
			Instantiate( ball, new Vector3(arrayHead + i, height, 0), Quaternion.identity );
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonDown (0)) {
			//print ("Hello, world!");
			//Instantiate( ball );	//create a clone of "ball"

			Instantiate( ball, new Vector3(0, height, 0), Quaternion.identity );	//create a clone at "height"
		}
	}
}
