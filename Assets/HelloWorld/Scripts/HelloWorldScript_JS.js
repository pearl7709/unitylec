﻿#pragma strict

public var ball:GameObject;	//ball
public var height:int = 5;	//height a ball fall from

function Start () {
	//print("Hello, world!");	//hello, world
	
	/* create ball array */
	var arrayHead:int = -2;	//Ball array starts from left
	for( var i:int = 0; i < 5; i++ ){
		Instantiate( ball, new Vector3(arrayHead + i, height, 0), Quaternion.identity );
	}
}

function Update () {
	if( Input.GetMouseButtonDown(0) ){
		//print("Hello, world!");	//hello, world
		//Instantiate( ball );	//create a clone of "ball"
		
		Instantiate( ball, new Vector3(0, height, 0), Quaternion.identity );	//create a clone at "height"
	}
}