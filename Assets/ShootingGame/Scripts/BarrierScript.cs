﻿using UnityEngine;
using System.Collections;

public class BarrierScript : MonoBehaviour {
	public float speed = 2.0f;
	public float widthMovingArea = 4.0f;
	public int maxHP = 5;
	private int healthPoint;
	private int direction = 1;

	// Use this for initialization
	void Start () {
		healthPoint = maxHP;
	}

	// Update is called once per frame
	void Update () {
		if (transform.position.x >= widthMovingArea || transform.position.x <= -widthMovingArea){
			direction = -direction;
		}
		transform.Translate( transform.right * direction * speed * Time.deltaTime );
		
	}

	void Damage(){
		print ("I'm not Target!");
		healthPoint--;

		/* When health point becomes 0, call "Dead" */
		if (healthPoint <= 0) {
			Dead();
		}
	}

	void Dead(){
		Application.LoadLevel( Application.loadedLevel );	//reload scene
		Destroy(gameObject);
	}
}
