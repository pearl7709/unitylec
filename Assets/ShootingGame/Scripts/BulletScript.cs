﻿using UnityEngine;
using System.Collections;

public class BulletScript : MonoBehaviour {

	/*   Call this function when the bullet collide with what has "Collider", 
	 * which is checked "Is Trigger".
	 *   At least one of them(the bullet and collided Object) must have Rigidbody.
	 */
	void OnTriggerEnter( Collider collider ){
		collider.gameObject.SendMessage( "Dead" );
		Destroy(gameObject);
	}

	/*   Call this function when the bullet collide with what has "Collider", 
	 * which is NOT checked "Is Trigger".
	 *   At least one of them(the bullet and collided Object) must have Rigidbody,
	 * which is NOT checked "Is Kinematic", 
	 * and the collision must have been happen by PHYSICAL MOVING.
	 */
	void OnCollisionEnter( Collision collision ){
		collision.gameObject.SendMessage( "Damage", SendMessageOptions.DontRequireReceiver );
	}
}
