﻿using UnityEngine;
using System.Collections;

public class TargetScript : MonoBehaviour {

	public float rotateSpeed = 60.0f;

	// Update is called once per frame
	void Update () {
		transform.Rotate( new Vector3( rotateSpeed * Time.deltaTime, 0, rotateSpeed * Time.deltaTime / 2f ) );
	}

	/* Dead function */
	void Dead(){
		transform.parent.SendMessage ("CountNumber");
		Destroy(gameObject);
	}
}
