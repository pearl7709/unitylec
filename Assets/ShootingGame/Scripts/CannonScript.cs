﻿using UnityEngine;
using System.Collections;

public class CannonScript : MonoBehaviour {

	public GameObject bullet;
	public float power = 1000.0f;	// Shot power
	public float moveSpeed = 2.0f;
	public float pitchSpeed = 20.0f;
	public float destroyBulletTime = 3.0f;
	
	// Update is called once per frame
	void Update () {
		/* Control position and angular */
		float valueHorizontal = Input.GetAxis("Horizontal");	//left/right key
		float valueVertical = Input.GetAxis("Vertical");	//up/down key
		transform.Translate( valueHorizontal * moveSpeed * Time.deltaTime, 0, 0);
		transform.Rotate( new Vector3(-valueVertical * pitchSpeed * Time.deltaTime, 0, 0) );

		/* Fire! */
		if( Input.GetButtonDown("Fire1") ){
			GameObject obj = Instantiate( bullet, transform.position, Quaternion.identity ) as GameObject;
			Rigidbody rb = obj.GetComponent<Rigidbody>();
			rb.AddForce( transform.up * power);

			Destroy( obj, destroyBulletTime );
		}
	}
}
