﻿using UnityEngine;
using System.Collections;

public class TargetGroupScript : MonoBehaviour {

	public GameObject target;
	private GameObject[] targetGroup;
	private int maxNumberOfTarget = 5;
	private int numberOfTarget;
	private int arrayHead = -4;

	// Use this for initialization
	void Start () {
		targetGroup = new GameObject[maxNumberOfTarget];
		numberOfTarget = maxNumberOfTarget;
		for( int i = 0; i < maxNumberOfTarget; i++ ){
			targetGroup[i] = Instantiate( target, transform.position + new Vector3(arrayHead + i*2, 0, 0), Quaternion.identity ) as GameObject;
			targetGroup[i].transform.parent = transform;
		}
	}

	void CountNumber(){
		numberOfTarget--;
		if(numberOfTarget <= 0 ){
			Application.LoadLevel( Application.loadedLevel );	//reload scene
		}
	}
}
